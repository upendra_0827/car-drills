
const below_year_cars = (inventory,year) => {                      // declaring the function

    if (Array.isArray(inventory)) {                            // Checking if the input is array

        let year_arr = inventory => {                             // function to collect year list of cars
            let years = []

            for (let car=0 ; car<inventory.length ; car++) {       // iterating the inventory
                years.push(inventory[car]['car_year'])               // pushing into years array
            }
            return years
        }

        let old_cars = []                                          // array to collect data of required years
        for (let element of year_arr(inventory)) {
            if (element<year) {                                   // to check if the year is passing the condition
                old_cars.push(element)
            }
        } return (`Number of cars is ${old_cars.length}`)  
        } else {
            return 'Invalid input'
        }
    
}

module.exports = below_year_cars                         // exporting the function