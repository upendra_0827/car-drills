
const last_car = inventory => {                                               // declaring the function

        if (Array.isArray(inventory)) {
                last_car_info = inventory[inventory.length-1]                           // info of last last car 
                return (`Last car is a ${last_car_info['car_make']} ${last_car_info['car_model']}.`) 
        } else {
                return 'Invalid input'
        }
}

module.exports = last_car                                            // exporting the function