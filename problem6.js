
let BMW_Audi = inventory => {                                   // declaring the function            

    if (Array.isArray(inventory)) {                      // checking if the input is array
        let BMWAndAudi = []                                   // array to collect output data
        for (let car of inventory) {                         // iterating the array
            if (car['car_make']==='Audi' || car['car_make']==='BMW') {          // checking if the car is BMW or Audi
                BMWAndAudi.push(car)                          // pushing the output data into empty array 
            }
        }
        return BMWAndAudi
    } else {
        return 'Invalid input'
        }
}

module.exports = BMW_Audi                              // exporting the function