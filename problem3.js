
const alphabetic_order = inventory => {                      // declaring the function
    
    let car_models = []                                      // to accumulate the result

    if (Array.isArray(inventory)) {                          // to check if the input is array
        for (let car=0 ; car<inventory.length ; car++) {         // iterating the inventory
            car_models.push(inventory[car]['car_model'])          // pushing the data into empty array
        } 
        return (car_models.sort())                             // alphabetically sorting the data
    } else {
        return 'Invalid input'
    }
}

module.exports = alphabetic_order                    // exporting the function