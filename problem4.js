
let year_arr = inventory => {                                // declaring the function
    let years = []                                        // to store the years data

    if (Array.isArray(inventory)) {                               // to check if the input is array
        for (let car=0 ; car<inventory.length ; car++) {           // itearting the input
            years.push(inventory[car]['car_year'])          // pushing the data into empty array
        }
        return years                                  // returning the answer
    } else {
        return 'Invalid input'
    }
}

module.exports = year_arr                            // exporting the function