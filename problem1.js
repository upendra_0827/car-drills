
let findd = (inventory,id_number) => {                              // declaring the find function

    if (Array.isArray(inventory)) {                              // checking if the input is array
        for (let car=0 ; car<inventory.length ; car++) {              // iterating the investory
            if (inventory[car]['id'] === id_number) {                       // checking our required car id
                return (`Car ${id_number} is a ${inventory[car]['car_year']}, ${inventory[car]['car_make']}, ${inventory[car]['car_model']}.`)  // returning the result
            }
        }
    } else {
        return 'Invalid input'
    }
}

module.exports = findd                                         // exporting the function